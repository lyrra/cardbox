<?php

namespace Hazizurin\Cardbox\Tests;

use PHPUnit\Framework\TestCase;
use Hazizurin\Cardbox\CardboxFactory;

class CardboxFactoryTest extends TestCase
{
    /** @test */
    public function it_returns_a_random_type()
    {
        $types = new CardboxFactory([
            'Layout',
        ]);

        $type = $types->getRandomType();

        $this->assertSame('Layout', $type);
    }

    /** @test */
    public function it_returns_a_predefined_type()
    {
        $cardtypes = [
            'Layout',
            'Template',
            'Component',
            'Color',
            'Theme',
        ];

        $types = new CardboxFactory();

        $type = $types->getRandomType();

        $this->assertContains($type, $cardtypes);
    }
}
