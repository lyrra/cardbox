# Cardbox

A sandbox of web cards.

## Installation
composer.json
### path
```json
    "repositories": [
        {
            "type": "path",
            "url": "../../hazizurin/cardbox"
        }
    ]
```
### vcs
```json
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/lyrra/cardbox"
        }
    ]
```
```bash
composer require hazizurin/alita:v0.1.x-dev
```

## Packagist
https://packagist.org/packages/hazizurin/cardbox

## Travis CI
None.

## Style CI
https://gitlab.styleci.io/accounts/4723884

## License
[MIT](./LICENCE.md)