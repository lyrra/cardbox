<?php

namespace Hazizurin\Cardbox;

class CardboxFactory
{
    protected $types = [
        'Layout',
        'Template',
        'Component',
        'Color',
        'Theme',
    ];

    public function __construct(array $types = null)
    {
        if ($types) {
            $this->types = $types;
        }
    }

    public function getRandomType()
    {
        return $this->types[array_rand($this->types)];
    }
}
